# Kompass

**Imported from a repository on [Github](https://github.com/Gnu-Bricoleur/Kompass)**

**Future work will be done here**

Compass application for Manjaro Plasma OS, tested on a Pinephone Braveheart edition.
It should be easy to compile and for other mobile OSs provided they have a graphical interface based on QML and Kirigami.
It should work on other versions of the Pinephone with minor modifications (currently, it's looking for an iio enabled lis3mdl in the device tree) and also other Linux smartphones (or any Linux computer with a magnetometer).

[Phabrikator tasks](https://phabricator.kde.org/T8905)

[Invent repository](https://invent.kde.org/smigaud/kompass)

# How to compile and test
Clone the repository, install the required dependencies for the compilation and compile
```
git clone git@github.com:Gnu-Bricoleur/Kompass.git
sudo pacman -Syu git cmake make extra-cmake-modules base-devel libiio
mkdir build
cd build
cmake ..
make
sudo make install
```
Increase the sampling frequency from the default (1Hz) to 80Hz to improve the experience
```
sudo iio_attr -d lis3mdl sampling_frequency 80
```
Launch the app
```
kompass
```
An icon in the launcher should also be available to launch the application

**Turn the smartphone at least one full turn to calibrate**

# TODO :
- [x] Add an icon
- [ ] Add "Info" page
- [ ] Add a "Advanced" page with more info from the sensor
- [ ] Add tilt compenssation algorithm
- [x] Add sane default for the scaling
- [ ] Add a message asking to turn the smartphone to complete calibration
- [ ] Improve UI
- [ ] Support other magnetometers than Braveheart Pinephone's (at least the magnetometers from other versions of the Pinephone)


# Contributors
Sylvain Migaud <sylvain@migaud.net>

Jonah Brüchert <jbb@kaidan.im>

Carl Schwan <carl@carlschwan.eu>

# Licence
The files in this repository are placed under the following licence except when otherwise mentioned in the header : GPL-2.0-or-later

The pictures are from wikimedia : [icon](https://commons.wikimedia.org/wiki/File:Compass_Card.svg) & [compass hand](https://commons.wikimedia.org/wiki/File:Compass_Card_B%2BW.svg)

They are placed under the Creative Commons Attribution-Share Alike 3.0 Unported license. 
