// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2021 Sylvain Migaud <sylvain@migaud.net>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#include "compass.h"

#include <chrono>
#include <cmath>

#include <iio.h>

using namespace std::chrono_literals;

Compass::Compass(QObject *parent)
    : QObject(parent)
{
    // Create iio structure and contexts
    struct iio_context *ctx = iio_create_default_context();
    if (!ctx) {
        setAvailable(false);
        return;
    }
    struct iio_device *dev = iio_context_find_device(ctx, "lis3mdl");
    if (!dev) {
        setAvailable(false);
        return;
    }

    // Find the 3 magnetometers channels
    m_channels = IIOChannels {
        iio_device_find_channel(dev, "magn_x", false),
        iio_device_find_channel(dev, "magn_y", false),
        iio_device_find_channel(dev, "magn_z", false)
    };

    setAvailable(true);

    // Start a timer to call periodically a function reading the magnetometer values
    m_timer.setInterval(50ms);
    m_timer.start();

    connect(&m_timer, &QTimer::timeout, this, &Compass::updateCompass);
}

void Compass::updateCompass() {
    double ax, ay, az;
    int angle = 0;

    // Read the magnetic field strength on the 3 axis from the magnetometer
    iio_channel_attr_read_double(m_channels.chx, "raw", &ax);
    iio_channel_attr_read_double(m_channels.chy, "raw", &ay);
    iio_channel_attr_read_double(m_channels.chz, "raw", &az);

    // If a value outside of the default range is read, then update the range to improve the scaling
    // Erratic reading seems to occur from time to time, calibrating the range with only a tenth of the new min/max
    // helps preventing those reading to mess too badly the calibration, proper filtering would be even better
    if (ax < m_minx){
        m_minx = (ax + m_minx * 9)/10;
    }
    if (ax > m_maxx){
        m_maxx = (ax + m_maxx * 9)/10;
    }
    if (ay < m_miny){
        m_miny = (ay + m_miny * 9)/10;
    }
    if (ay > m_maxy){
        m_maxy = (ay + m_maxy * 9)/10;
    }

    // Scale the readings on axes X and Y
    ax = ((ax - m_minx) / (m_maxx - m_minx) * M_PI) - 1.55;
    ay = ((ay - m_miny) / (m_maxy - m_miny) * M_PI) - 1.55;

    // Calculate the heading
    angle = std::atan2(ay, ax) * 180 / M_PI;
    angle = angle + 180;

    // Calculate the 360 modulo of the angle
    while(angle > 360) {
        angle -= 360;
    }
    while (angle < 0) {
        angle += 360;
    }
    angle = 360 - angle;

    // Set the compass hand angle
    setRotation(angle);
}

int Compass::rotation() const
{
    return m_rotation;
}

void Compass::setRotation(int angle)
{
    if (m_rotation == angle)
        return;

    m_rotation = angle;
    Q_EMIT rotationChanged();
}

bool Compass::available() const
{
    return m_available;
}

void Compass::setAvailable(bool available)
{
    m_available = available;
    Q_EMIT availableChanged();
}
