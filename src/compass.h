// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2021 Sylvain Migaud <sylvain@migaud.net>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#pragma once

#include <QObject>
#include <QTimer>

struct IIOChannels {
    struct iio_channel *chx = nullptr;
    struct iio_channel *chy = nullptr;
    struct iio_channel *chz = nullptr;
};

class Compass : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int rotation READ rotation NOTIFY rotationChanged)
    Q_PROPERTY(bool available READ available NOTIFY availableChanged)

public:
    explicit Compass(QObject *parent = nullptr);

    int rotation() const;
    void setRotation(int angle);
    Q_SIGNAL void rotationChanged();

    bool available() const;
    void setAvailable(bool available);
    Q_SIGNAL void availableChanged();

private:
    Q_SLOT void updateCompass();

    QTimer m_timer;
    IIOChannels m_channels;
    // Set sane default values for the possible range of readings for scaling
    // Their purpose is to enable instant azimuth display without previous calibration
    // Incorrect default startup values can make the compass malfunction and jump
    // It is needed to check if those default values are ok for all devices
    double m_minx = -6000.0;
    double m_maxx = -4000.0;
    double m_miny = 0.0;
    double m_maxy = 1500.0;
    int m_rotation = 90;
    bool m_available = true;
};
