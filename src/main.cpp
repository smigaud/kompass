// SPDX-FileCopyrightText: 2021 Sylvain Migaud <sylvain@migaud.net>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <QDebug>
#include <QTimer>
#include <KLocalizedContext>

#include "compass.h"

constexpr auto URI = "org.kde.kompass";

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    //QCoreApplication::setOrganizationName("KDE");
    //QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("Kompass");

    // Load the main page
    QQmlApplicationEngine engine;

    qmlRegisterType<Compass>(URI, 1, 0, "Compass");
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));

    engine.load(QUrl(QStringLiteral("qrc:///KompassPage.qml")));

    // Launch the application
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }
    int ret = app.exec();
    return ret;
}
